import argparse
import functools
import os
import random
from datetime import datetime
import numpy
import torch
import yaml
from termcolor import cprint


def parse_params(main_func):
    @functools.wraps(main_func)
    def wrapper(*args, **kwargs):
        parser = argparse.ArgumentParser()
        parser.add_argument('settings', help='path to a YAML settings file')
        parser.add_argument('updates', nargs='*', type=str,
                            help='optional parameters updates of the form \'param=value\'')
        args = parser.parse_args()

        if is_settings_file(args.settings):
            with open(args.settings, 'r') as f:
                params = yaml.safe_load(f)
        else:
            raise ValueError(f'Failed to parse {args.settings}')

        post_hooks = [verbosity_check, create_results_dir, save_settings]
        for hook in post_hooks:
            hook(params)
        return main_func(**params)
    return wrapper


def create_results_dir(params):
    if 'results_dir' in params:
        params['results_dir'] = params['results_dir'].replace('{timestamp}', datetime.now().strftime('%y%h%d/%H-%M-%S'))
        params['results_dir'] = os.path.join(os.getcwd(), params['results_dir'])
        os.makedirs(params['results_dir'], exist_ok=True)
    else:
        raise AttributeError(f"'results_dir' not set")


def save_settings(params):
    if 'results_dir' in params:
        filename = os.path.join(params['results_dir'], 'settings.yml')
        with open(filename, 'w') as stream:
            stream.write(yaml.dump(params, sort_keys=False))
    if params.get('verbosity', 0) >= 2:
        cprint('Running with settings:', 'blue')
        print(yaml.dump(params, sort_keys=False))


def verbosity_check(params):
    if 'verbosity' not in params:
        params['verbosity'] = 0
        print(f"verbosity set to {params['verbosity']}")


def is_settings_file(filename):
    _, file_ext = os.path.splitext(filename)
    if file_ext in ['.yaml', '.yml']:
        if not os.path.isfile(filename):
            raise FileNotFoundError(f"{filename}: No such script found")
        return True
    else:
        return False


def set_seed(seed, incr):
    if seed is not None:
        seed += incr
        random.seed(seed)
        torch.manual_seed(seed)
        numpy.random.seed(seed)
