#define MAXN 0 //just an identifier - do not change!
#define MAXQ 1 //just an identifier - do not change!
#define MAXG 2 //just an identifier - do not change!

struct GRAPH;
struct PAR_MANAGER
{
	union
	{
		int par[3];
		struct
		{
			int n, q, g;
		};
	};

	GRAPH* G = NULL;

	void Init(GRAPH* G,
	          const std::vector<int>& memory,
	          const std::vector<double>& time)
	{
		if(this->G)
		{
			throw std::runtime_error("Trying to create two instances of GRAPH in parallel!");
		}

		this->G = G;
		n = memory.size();
		g = time.size();
		q = 0;

		for(int i = 0; i < n; i++)
		{
			q += memory[i];
		}
	}

	void InitFinished()
	{
		G = NULL;
	}
}
ParManager;

template<typename T, int S>
struct ARRAY1D
{
	int size = 0;
	T* a = NULL;

	ARRAY1D()
	{
//		if(!(((void*) this >= ParManager.G) && ((void*) this < ParManager.G + 1)))
//		{
//			throw std::runtime_error("Invalid array creation!");
//		}

		size = ParManager.par[S];
		a = new T[size]();
	}

	~ARRAY1D()
	{
		delete[] a;
	}

	ARRAY1D& operator = (ARRAY1D& other)
	{
		for(int i = 0; i < size; i++)
		{
			(*this)[i] = other[i];
		}

		return *this;
	}

	T& operator [] (int i)
	{
		return a[i];
	}
};

template<typename T, int S1, int S2>
struct ARRAY2D
{
	int size1 = 0;
	int size2 = 0;
	T* a = NULL;

	ARRAY2D()
	{
//		if(!(((void*) this >= ParManager.G) && ((void*) this < ParManager.G + 1)))
//		{
//			throw std::runtime_error("Invalid array creation!");
//		}

		size1 = ParManager.par[S1];
		size2 = ParManager.par[S2];
		a = new T[size1 * size2];
	}

	~ARRAY2D()
	{
		delete[] a;
	}

	ARRAY2D& operator = (ARRAY2D& other)
	{
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				(*this)[i][j] = other[i][j];
			}
		}

		return *this;
	}

	T* operator [] (int i)
	{
		return a + (i * size2);
	}
};

template<typename T, int S1, int S2, int S3>
struct ARRAY3D
{
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	T* a = NULL;

	ARRAY3D()
	{
//		if(!(((void*) this >= ParManager.G) && ((void*) this < ParManager.G + 1)))
//		{
//			throw std::runtime_error("Invalid array creation!");
//		}

		size1 = ParManager.par[S1];
		size2 = ParManager.par[S2];
		size3 = ParManager.par[S3];
		a = new T[size1 * size2 * size3];
	}

	~ARRAY3D()
	{
		delete[] a;
	}

	ARRAY3D& operator = (ARRAY3D& other)
	{
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				for(int k = 0; k < size3; k++)
				{
					(*this)[i][j][k] = other[i][j][k];
				}
			}
		}

		return *this;
	}

	struct HELPER
	{
		ARRAY3D* p;
		int i;

		HELPER(ARRAY3D* p, int i): p(p), i(i) {}
		T* operator [] (int j) {return p->a + (((i * p->size2) + j) * p->size3);}
	};

	HELPER operator [] (int i)
	{
		return HELPER(this, i);
	}
};
