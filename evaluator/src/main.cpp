#include <torch/extension.h>
#include <ATen/ATen.h>

#include "patrol.h"

using namespace pybind11::literals;

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
	py::class_<GRAPH>(m, "Game")
	        .def(py::init<std::vector<int>,
	                      std::vector<double>,
	                      std::vector<double>,
	                      std::vector<double>,
	                      std::vector<EDGE_TYPE>>(),
	             "Initializes the game parameters",
	             "memory"_a,
	             "attack_len"_a,
	             "value"_a,
	             "blindness"_a,
		         "edges"_a)
	        .def("get_mask", &GRAPH::PyGetMask)
	        .def("eval_strategy", &GRAPH::PyEvaluateStrategy, "strat"_a, "eps"_a, "pmt"_a = PATH_MERGE_THRESHOLD)
	        .def("compute_gradient", &GRAPH::PyComputeGradient);

	py::class_<SWITCHING_GRAPH>(m, "SwitchingGame")
	        .def(py::init<std::vector<int>,
	                      std::vector<double>,
	                      std::vector<double>,
	                      std::vector<double>,
	                      std::vector<EDGE_TYPE>,
	                      std::vector<double>,
	                      std::vector<double>,
	                      std::vector<EDGE_TYPE>>(),
	             "Initializes the game parameters",
	             "memory"_a,
	             "attack_len"_a,
	             "value1"_a,
	             "blindness1"_a,
	             "edges1"_a,
	             "value2"_a,
	             "blindness2"_a,
	             "edges2"_a)
	        .def("eval_switching_strategy", &SWITCHING_GRAPH::PyEvaluateSwitchingStrategy, "strat1"_a, "strat2"_a, "pmt"_a = PATH_MERGE_THRESHOLD);
}
