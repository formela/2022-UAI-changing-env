from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension

setup(name='patrolling_strategy_evaluation',
      ext_modules=[CppExtension('patrolling_strategy_evaluation', ['src/main.cpp'])],
      cmdclass={'build_ext': BuildExtension},
      description="Patrolling strategy evaluation")
