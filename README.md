# On-the-fly Adaptation of Patrolling Strategies in Changing Environments (UAI 2022)

Implementation of an efficient strategy synthesis algorithm for robot motion planning problem in changing environment. 

We consider the problem of adaptation of patrolling strategies in a changing environment where the topology of Defender's moves and the importance of guarded targets unpredictably change. The Defender must instantly switch to a new strategy optimized for the new environment, not disrupting the ongoing patrolling task, and the new strategy must be computed promptly under all circumstances. Since strategy switching may cause unintended security risks compromising the achieved protection, our solution includes mechanisms for detecting and mitigating this problem.

To appear in [UAI 2022](https://www.auai.org/uai2022/).

Up-to-date version of Regstar is at [gitlab.fi.muni.cz/formela/regstar](https://gitlab.fi.muni.cz/formela/regstar/).

# Strategy Optimization

## Installation

Create virtual environment (`pipenv`) and install all packages:

1. Make sure you have `python 3.6` and `pipenv` installed
1. Install packages from pipfile (at your own risk with `--skip-lock`)
   > python3 -m pipenv install
1. Enter the virtualenv 
   > python3 -m pipenv shell
1. Install local evaluation module 
   > cd evaluator && python setup.py install && cd -

## Execution

Enter the virtual environment:
> python3 -m pipenv shell

Run the optimization:
> python3 train.py path/to/your/settings.yml

where `path/to/your/settings.yml` is the path to your settings file.


## Experiments setting

Experiment parameters are stored in `YAML` setting files.
They contain:

* `graph_params` patrolling graph specification
* `results_dir: results/experiment_name/{timestamp}` path to store the results. The use of `{timestamp}` is available.
* `epochs: <int>` number of optimization trials starting from checkpoint or  random initialization
* `seed: null` set seed for reproducibility
* `verbosity: 0-3` 0=silent, 1=prints val per epoch, 2=prints val per step, 3=prints all (debug mode)
* `trainer_params` optimization parameters
* `game_params` patrolling game parameters
* `logger_params` logging options


### Available settings

#### Comparison to Regstar

Directory `settings/comparison_to_regstar` contains templates of experiments on Montreal map (`ex2.yml`)
and on Office buildings (`ex3a.yml`, `ex3b.yml`, `ex3c.yml`, `ex3d.yml`).

The experiments in the paper contain results
with various memory sizes; to reproduce them, modify `memory` in the settings appropriately.

#### Vancouver downtown

Directory `settings/vancouver` contains files needed to perform the experiments with changing environment.

Data for generation of the Vancouver patrolling graph are stored in `kml_maps/vancouver.kml`.

* `take_checkpoint.yml` was run to find strong strategy in the original graph.
For reproducibility, it is stored in `checkpoints/strategy_sd.pth` 
  
The next settings modifies the graph and runs new optimizations (from sratch or from checkpoint).
Follow the comments in these files to reproduce all the possible environmental changes.

* `shake_costs` for nodes cost modifications
* `shake_edge_len` for edge length modifications
* `remove_k_edges` for edge removal

      
## Results

The results, visualizations and logs are stored in `results_dir`. They include:

* `settings.yml` A copy of the configuration used
* `stats_epoch.csv` Detailed statistics for all epochs
* `stats_final.csv` Statistics summary over all epochs
* `stats_switch.csv` Security gap logs (if `graph_params.modifiers` set)
* `graph.pdf` Basic patrolling graph visualization

*   If `logger_params.plot=True`
    * `ascend_progress.pdf` Plots of strategy values during optimization
    * `avg_times.pdf` Plot of average runtimes
    * `epoch_statistics.pdf` Epoch statistics visualizations
  
*   If `logger_params.save_checkpoints=True`
    * `strategy_sd.pth` State dict of the best strategy found

### Tensorboard logs

A tensorboardx log files are stored in `results_dir/tensorboard/epoch_<int>`. In the virtual environment, run

> tensorboard --logdir results_dir

to monitor the ascend progress during optimization.
