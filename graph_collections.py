import networkx
from geopy.distance import distance as geopy_distance

from itertools import product, combinations
import random, os
import math

"""
    Add your favourite graph here!
    -> don't forget to add it to 'graphs_dict' to make it available in settings
"""

def office_building(floors, halls, office_att=None, hall_att=None):
    office_attributes = dict(value=100, attack_len=112, blindness=0.0, memory=1, target=True)
    office_attributes.update(office_att or {})
    hall_attributes = dict(memory=4, target=False)
    hall_attributes.update(hall_att or {})
    graph = networkx.Graph()
    nodes = []
    edges = []
    for floor in range(floors):
        for hall in range(halls):
            up = f'Office_{floor}_{hall}up'
            dn = f'Office_{floor}_{hall}dn'
            cr = f'Hall_{floor}_{hall}'
            nodes += [(up, office_attributes),
                      (dn, office_attributes),
                      (cr, hall_attributes)]
            edges += [(cr, up, dict(len=5)),
                      (cr, dn, dict(len=5))]

        le = f'Office_{floor}_0le'
        ri = f'Office_{floor}_{halls - 1}ri'
        nodes += [(le, office_attributes),
                  (ri, office_attributes)]
        edges += [(le, f'Hall_{floor}_0', dict(len=5)),
                  (ri, f'Hall_{floor}_{halls - 1}', dict(len=5))]

        for hall in range(halls - 1):
            edges.append((f'Hall_{floor}_{hall}', f'Hall_{floor}_{hall + 1}', dict(len=2)))

    for floor in range(floors - 1):
        edges += [(f'Hall_{floor}_0', f'Hall_{floor + 1}_0', dict(len=10)),
                  (f'Hall_{floor}_{halls - 1}', f'Hall_{floor + 1}_{halls - 1}', dict(len=10))]

    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    graph.name = f'office_building_{floors}_f_{halls}_h'

    return graph


def distance(from_vert, to_vert, taxicab=True, distance_unit=None) -> float:
    """
    @param from_vert: pair of the source coordinates
    @param to_vert: pair of the target coordinates
    @param taxicab: usage of taxicab or Euclidean metric
    @param distance_unit: if not None, used as a parameter for convergence from GPS coordinates
                       (e.g., 'm' or 'meters', 'mi' or 'miles', 'km' or 'kilometers')
    @return:
    """
    if distance_unit is None:
        if taxicab:
            return abs(from_vert[0] - to_vert[0]) + abs(from_vert[1] - to_vert[1])
        else:
            return math.sqrt((from_vert[0] - to_vert[0]) ** 2 + (from_vert[1] - to_vert[1]) ** 2)

    if taxicab:
        d = geopy_distance(from_vert, (to_vert[0], from_vert[1])) + \
            geopy_distance(from_vert, (from_vert[0], to_vert[1]))
    else:
        d = geopy_distance(from_vert, to_vert)

    return getattr(d, distance_unit)


def motreal(kml_file_name, node_att=None, distance_unit='meters', rounding=100, seed=13):
    from xml.dom import minidom
    mydoc = minidom.parse(os.path.join(os.getcwd(), kml_file_name))
    coordinates = mydoc.getElementsByTagName('coordinates')
    names = mydoc.getElementsByTagName('name')[1:]
    num_nodes = len(coordinates)
    chosen_nodes = []

    for elem in coordinates:
        (y, x, _) = elem.firstChild.data.strip().split(',')
        chosen_nodes.append((float(y), float(x)))

    edges_dist = [math.ceil(distance(from_vert, to_vert, True, distance_unit)/rounding)
                  for from_vert, to_vert in product(chosen_nodes, chosen_nodes)]

    max_edge = max(edges_dist)
    mean_edge = sum(edges_dist) / (num_nodes * (num_nodes - 1))
    attack_time = int(2 * max_edge + mean_edge)

    if seed:
        random.seed(seed)
    costs = [random.randint(180, 200) for i in range(num_nodes)]
    blinds = [random.randint(0, 20) / 100 for i in range(num_nodes)]

    nodes = []
    edges = []
    for i in range(num_nodes):
        from_name = f'{i}_{names[i].firstChild.data}'
        node_attributes = dict(value=costs[i], attack_len=attack_time, blindness=blinds[i], memory=4, target=True)
        node_attributes.update(node_att or {})
        nodes.append((from_name, node_attributes))
        for j in range(i + 1, num_nodes):
            edges.append((from_name, f'{j}_{names[j].firstChild.data}', dict(len=round(edges_dist[i * num_nodes + j]))))

    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)

    return graph


def kml_graph(kml_file_name, node_att=None, taxicab=False, distance_unit='meters', rounding=1, seed=None):
    """
    Generates graphs from a kml file - exported from Google maps.
    @param kml_file_name: path to the kml file
    @param node_att: optional attributes updating the default values
    @param taxicab: usage of taxicab or Euclidean metric
    @param distance_unit: if not None, used as a parameter for convergence from GPS coordinates
    @param rounding: distances rounding
    @param seed: the seed for the random generator
    @return:
    """
    if seed is not None:
        random.seed(seed)

    from xml.dom import minidom
    kml_file = minidom.parse(kml_file_name)
    # get names (the fist one is the Document name)
    names = kml_file.getElementsByTagName('name')[1:]
    names = [f'{i}_{name.firstChild.data}' for i, name in enumerate(names)]
    # get coordinates
    coordinates = kml_file.getElementsByTagName('coordinates')
    chosen_nodes = []
    for elem in coordinates:
        (x, y, _) = elem.firstChild.data.strip().split(',')
        # x and y are switched correctly, see documentation
        chosen_nodes.append((float(y), float(x)))

    edges_dist = [math.ceil(distance(from_node, to_node, taxicab, distance_unit)/rounding)
                  for from_node, to_node in combinations(chosen_nodes, 2)]

    attack_time = int(2 * max(edges_dist) + sum(edges_dist) / len(edges_dist))

    nodes = []
    for name in names:
        node_attributes = dict(value=random.randint(90, 100),
                               attack_len=attack_time,
                               blindness=0.0, memory=4, target=True)
        node_attributes.update(node_att or {})
        nodes.append((name, node_attributes))

    edges = []
    for (from_name, to_name), dist in zip(combinations(names, 2), edges_dist):
        edge_attributes = dict(len=round(dist))
        edges.append((from_name, to_name, edge_attributes))

    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)

    return graph


graphs_dict = {
    'office_building': office_building,
    'kml_graph': kml_graph,
    'motreal': motreal
}
