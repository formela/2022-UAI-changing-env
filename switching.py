import os
import pandas
import torch
from time import process_time
import patrolling_strategy_evaluation as pse


class SwitchingGame(object):
    def __init__(self, old_graph, new_graph, old_strategy, old_in_old_val, old_in_new_val, log_dir, verbose=False):
        self.log_dir = log_dir
        self.verbose = verbose
        self.old_graph = old_graph
        self.new_graph = new_graph
        self.old_strategy = torch.from_numpy(old_strategy)
        self.old_steal = old_graph.max_val - old_in_old_val
        self.old_in_old = old_in_old_val
        self.old_in_new = old_in_new_val
        self.new_val = None
        self.gap = None
        self.switch_time = None
        old_raw = old_graph.get_raw_data()
        new_raw = new_graph.get_raw_data()
        assert old_raw['memory'] == new_raw['memory']
        assert old_raw['attack_len'] == new_raw['attack_len']
        self.switching_game = pse.SwitchingGame(
            memory=old_raw['memory'], attack_len=old_raw['attack_len'],
            value1=old_raw['value'], blindness1=old_raw['blindness'], edges1=old_raw['edges'],
            value2=new_raw['value'], blindness2=new_raw['blindness'], edges2=new_raw['edges'])

    def eval(self, new_strategy, new_val):
        self.new_val = new_val
        new_strategy = torch.from_numpy(new_strategy)
        new_steal = self.new_graph.max_val - new_val
        start = process_time()
        gap_steal = self.switching_game.eval_switching_strategy(self.old_strategy, new_strategy)
        self.switch_time = process_time() - start
        gap_steal *= self.new_graph.max_val
        self.gap = max(0, gap_steal - max(self.old_steal, new_steal))

    def dump(self):
        results = dict(
            old_in_old_val=self.old_in_old / self.old_graph.max_val,
            old_in_new_val=self.old_in_new / self.new_graph.max_val,
            new_in_new_val=self.new_val / self.new_graph.max_val,
            gap=self.gap / self.new_graph.max_val,
            switch_time=self.switch_time
        )
        pandas.Series(results).to_csv(os.path.join(self.log_dir, 'stats_switch.csv'), header=False)
        if self.verbose:
            self.print_results(results)

    @staticmethod
    def print_results(data):
        str_fmt = 'Switching:\told in old val={old_in_old_val:.3f}'
        str_fmt += '\told in new val={old_in_new_val:.3f}'
        str_fmt += '\tnew in new val={new_in_new_val:.3f}'
        str_fmt += '\tgap={gap:.3f}'
        str_fmt += '\ttime={switch_time:.2f}'
        print(str_fmt.format(**data))
