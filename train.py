from graph_utils import PatrollingGraph
from logs import Logger, TrainLogger
from model import PatrollingGame
from trainer import StrategyTrainer
from utils import set_seed, parse_params
from switching import SwitchingGame


@parse_params
def main(seed, verbosity, results_dir, epochs, graph_params, game_params, trainer_params, logger_params=None):
    logger_params = logger_params or {}

    modifiers = graph_params.pop('modifiers', None)
    new_graph = PatrollingGraph(**graph_params, modifiers=modifiers, results_dir=results_dir, verbose=(verbosity >= 2))

    logger = Logger(results_dir, **logger_params, verbose=(verbosity >= 1))

    for epoch in range(epochs):
        set_seed(seed, epoch)
        game = PatrollingGame(new_graph, **game_params, verbose=(verbosity >= 3))
        train_logger = TrainLogger(epoch, tensorboard=True, log_dir=results_dir)
        trainer = StrategyTrainer(game, train_logger, **trainer_params, verbose=(verbosity >= 3))
        metrics = trainer.train()
        logger.update(*metrics)

    val, strategy = logger.dump()

    if modifiers is not None:
        old_graph = PatrollingGraph(**graph_params, modifiers=None)
        checkpoint = game_params.pop('checkpoint', None)
        old_in_old_val, old_strategy = PatrollingGame(old_graph, **game_params, checkpoint=checkpoint).eval()
        old_in_new_val, _ = PatrollingGame(new_graph, **game_params, checkpoint=checkpoint).eval()

        switching_game = SwitchingGame(old_graph, new_graph, old_strategy, old_in_old_val, old_in_new_val,
                                       verbose=(verbosity >= 1), log_dir=results_dir)
        switching_game.eval(strategy, val)
        switching_game.dump()


if __name__ == '__main__':
    main()
