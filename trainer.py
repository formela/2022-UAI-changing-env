import math
from copy import deepcopy

import numpy as np
import torch
from torch.optim import Adam
from time import process_time
from termcolor import cprint


class StrategyTrainer(object):
    def __init__(self, game, logger, steps, eval_every=1, optimizer_params=None, verbose=False):
        optimizer_params = optimizer_params or {}
        self.verbose = verbose
        self.logger = logger
        self.logger.steps = steps
        self.step_num = 0
        self.val = 0.0
        self.strategy = None
        self.val_train = None
        self.loss = None
        self.times = None
        self.steps = steps
        self.eval_every = eval_every
        self.game = game
        self.optimizer = Adam(params=self.game.parameters(), **optimizer_params)

    def noise(self):
        rand_noise = torch.clamp(torch.randn_like(self.game.strategy_par.grad), -3.0, 3.0)
        step_noise = rand_noise / self.step_num
        self.game.strategy_par.grad += step_noise

    def eval(self):
        if self.step_num % self.eval_every == 0:
            self.val, self.strategy = self.game.eval()
            self.print_eval()

    def step(self):
        start_time = process_time()
        self.step_num += 1
        self.print_start_step()
        self.optimizer.zero_grad()
        self.loss, self.val_train = self.game.forward()
        self.print_step()
        forward_time = process_time()
        self.loss.backward()
        self.print_grad()
        backward_time = process_time()
        self.noise()
        self.optimizer.step()
        optimizer_time = process_time()
        self.eval()
        eval_time = process_time()
        self.times = dict(
            f_time=forward_time - start_time,
            b_time=backward_time - forward_time,
            o_time=optimizer_time - backward_time,
            e_time=eval_time - optimizer_time)
        self.log()

    def train(self):
        if self.steps == 0:
            start_time = process_time()
            self.eval()
            eval_time = process_time()
            self.logger.update(
                step_num=self.step_num,
                val=self.val,
                val_train=0,
                loss=0,
                strategy=self.strategy,
                state_dict=deepcopy(self.game.state_dict()),
                f_time=0,
                b_time=0,
                o_time=0,
                e_time=eval_time - start_time)
        while self.step_num < self.steps:
            self.step()
            if self.val == self.game.evaluator.max_value:
                break
        return self.logger.dump()

    def log(self):
        self.logger.update(
            step_num=self.step_num,
            val=self.val,
            val_train=self.val_train,
            loss=self.loss.item(),
            strategy=self.strategy,
            state_dict=deepcopy(self.game.state_dict()),
            **self.times)

    def print_eval(self):
        if self.verbose:
            cprint(f'step={self.step_num} (eval)\t val={self.val:.2f}', 'cyan')
            self.game.graph.print_strategy(self.strategy, style='matrix', precision=3)

    def print_start_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train) started', 'green')

    def print_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train)\t val={self.val_train:.2f}\t loss={self.loss:.2f}', 'green')

    def print_grad(self):
        if self.verbose:
            with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
                cprint(f'Strategy grad:', 'blue')
                print(' ', np.array2string(self.game.strategy_par.grad.numpy(), prefix='  '))
