import numpy
import pandas
import os
import torch
from time import process_time
from tensorboardX import SummaryWriter
from postprocess.plotting import (
    plot_statistics,
    plot_values,
    plot_times
)


class Logger(object):
    def __init__(self, log_dir, plot=False, save_logs=False, save_checkpoints=False, verbose=False):
        self.log_dir = log_dir
        self.verbose = verbose
        self.plot = plot
        self.save_logs = save_logs
        self.best_strategy = None
        self.best_strategy_sd = None
        self.best_val = float('-inf')
        self.save_checkpoints = save_checkpoints
        epoch_columns = ['epoch', 'steps', 'end_val', 'best_val', 'best_val_at', 'avg_val', 'epoch_time',
                         'avg_step_time', 'avg_forward_time', 'avg_backward_time', 'avg_optimizer_time',
                         'avg_eval_time']
        self.epoch_data = pandas.DataFrame(columns=epoch_columns)
        log_columns = ['epoch', 'step', 'val', 'val_train', 'loss', 'step_time', 'forward_time', 'backward_time',
                       'optimizer_time', 'eval_time']
        self.log_data = pandas.DataFrame(columns=log_columns)
        self.strategies = []
        self.start_time = process_time()

    def update(self, metrics, times, strategy, log_data):
        self.log_data = self.log_data.append(log_data, ignore_index=True)
        epoch_data = dict(**metrics, **times)

        if metrics['best_val'] > self.best_val:
            self.best_val = metrics['best_val']
            self.best_strategy, self.best_strategy_sd = strategy

        if self.verbose:
            self.print_epoch(epoch_data)

        epoch_df = pandas.DataFrame(epoch_data, index=[metrics['epoch']])
        self.epoch_data = self.epoch_data.append(epoch_df)

    def dump(self):
        total_time = process_time() - self.start_time
        for key in ['epoch', 'steps', 'best_val_at']:
            self.epoch_data[key] = self.epoch_data[key].astype('int')
        self.epoch_data.sort_index(inplace=True)
        self.epoch_data.to_csv(os.path.join(self.log_dir, 'stats_epoch.csv'), index=False)
        if self.save_logs:
            self.log_data.to_csv(os.path.join(self.log_dir, 'stats_full.csv'), index=False)

        final_data = dict(
            epochs=self.epoch_data['epoch'].max(),
            best_val=self.epoch_data['best_val'].max(),
            best_val_at_epoch=self.epoch_data['best_val'].argmax()+1,
            avg_epoch_time=self.epoch_data['epoch_time'].mean(),
            avg_step_time=self.epoch_data['avg_step_time'].mean(),
            total_time=total_time
        )
        final_data_series = pandas.Series(final_data)
        final_data_series.to_csv(os.path.join(self.log_dir, 'stats_final.csv'), header=False)

        if self.verbose:
            self.print_stats(final_data)

        if self.save_checkpoints:
            torch.save(self.best_strategy_sd, os.path.join(self.log_dir, f'strategy_sd.pth'))

        if self.plot:
            if final_data['epochs'] > 1:
                plot_statistics(self.epoch_data, os.path.join(self.log_dir, 'epoch_statistics.pdf'))
            plot_values(self.log_data, os.path.join(self.log_dir, 'ascend_progress.pdf'))
            plot_times(self.log_data, os.path.join(self.log_dir, 'avg_times.pdf'))

        return self.best_val, self.best_strategy

    @staticmethod
    def print_epoch(epoch_data):
        str_fmt = 'epoch {epoch}:\tval={end_val:.1f}'
        str_fmt += '\tmax val={best_val:.1f} at {best_val_at}'
        str_fmt += '\ttime={epoch_time:.2f}'
        str_fmt += '\tavg step time={avg_step_time:.3f}'
        print(str_fmt.format(**epoch_data))

    @staticmethod
    def print_stats(final_data):
        str_fmt = 'Finished with best val = {best_val:.1f} at epoch = {best_val_at_epoch}'
        str_fmt += '\navg epoch time\t= {avg_epoch_time:.2f}'
        str_fmt += '\navg step time\t= {avg_step_time:.3f}'
        str_fmt += '\ntotal time\t= {total_time:.1f}'
        print(str_fmt.format(**final_data))


class TrainLogger(object):
    def __init__(self, epoch, tensorboard=False, log_dir=None):
        self.epoch = epoch + 1
        self.steps = None
        self.val = []
        self.val_train = []
        self.loss = []
        self.forward_time = []
        self.backward_time = []
        self.optimizer_time = []
        self.eval_time = []
        self.step_time = []
        self.tb_logger = None
        self.best_val = float('-inf')
        self.strategy = None
        self.state_dict = None
        self.tensorboard = tensorboard
        if tensorboard:
            self.tb_logger = SummaryWriter(log_dir=os.path.join(log_dir, f'tensorboard/epoch_{epoch:02d}'))
        self.start_time = process_time()

    def update(self, step_num, val, val_train, loss,
               strategy, state_dict,
               f_time, b_time, o_time, e_time):
        if self.tb_logger is not None:
            self.tb_logger.add_scalar(f'val', val, step_num)
            self.tb_logger.add_scalar(f'val_train', val_train, step_num)
            self.tb_logger.add_scalar(f'loss', loss, step_num)
        if val > self.best_val:
            self.best_val = val
            self.strategy = strategy
            self.state_dict = state_dict
        self.val.append(val)
        self.val_train.append(val_train)
        self.loss.append(loss)
        self.forward_time.append(f_time)
        self.backward_time.append(b_time)
        self.optimizer_time.append(o_time)
        self.eval_time.append(e_time)

    def dump(self):
        epoch_time = process_time() - self.start_time
        if self.tensorboard:
            self.tb_logger.close()
        df = pandas.DataFrame(
            dict(
                epoch=self.epoch,
                val=self.val,
                val_train=self.val_train,
                loss=self.loss,
                forward_time=self.forward_time,
                backward_time=self.backward_time,
                optimizer_time=self.optimizer_time,
                eval_time=self.eval_time
            ))

        steps = len(self.val)
        df['step'] = range(1, steps + 1)
        df['step_time'] = df['forward_time'] + df['backward_time'] + df['optimizer_time'] + df['eval_time']

        metrics = dict(
            epoch=self.epoch,
            steps=steps,
            end_val=df['val'].iloc[-1],
            best_val=df['val'].max(),
            avg_val=(df['val'].sum() + (self.steps - steps) * df['val'].iloc[-1]) / self.steps,
            best_val_at=df['val'].argmax() + 1,
        )
        times = dict(
            epoch_time=epoch_time,
            avg_step_time=df['step_time'].mean(),
            avg_forward_time=df['forward_time'].mean(),
            avg_backward_time=df['backward_time'].mean(),
            avg_optimizer_time=df['optimizer_time'].mean(),
            avg_eval_time=df['eval_time'].mean(),
        )
        strategy = (
            self.strategy,
            self.state_dict
        )
        return metrics, times, strategy, df
