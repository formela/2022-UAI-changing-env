import math
import os
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, chain, combinations
from math import ceil


def plot_values(log_data, filename):
    fig, (ax_val, ax_val_train, ax_loss) = plt.subplots(3, 1, figsize=(10, 15))
    plots = dict(val=(ax_val, 'value'),
                 val_train=(ax_val_train, 'training value'),
                 loss=(ax_loss, 'loss'))

    for key, (ax, ylabel) in plots.items():
        ax.set(ylabel=ylabel)
        data = log_data.pivot(index='step', columns='epoch', values=key)
        data.plot(ax=ax, legend=False, grid=True, lw=0.5)

    fig.savefig(filename)


def plot_statistics(epoch_data, filename):
    fig, ((ax_best_val, ax_avg_val), (ax_best_val_at, ax_steps), (ax_time, ax_step_time)) = plt.subplots(3, 2, figsize=(
        10, 15))

    def stats_dict(key):
        return dict(mean=epoch_data[key].mean(),
                    std=epoch_data[key].std(),
                    median=epoch_data[key].median(),
                    max=epoch_data[key].max(),
                    min=epoch_data[key].min())

    str_fmt = '\n{min:.1f} ≤ {median:.1f} / {mean:.1f} ± {std:.1f} ≤ {max:.1f}'

    plots = dict(best_val=(ax_best_val, 'Best value', '#1f77b4'),
                 avg_val=(ax_avg_val, 'Average value', '#1f77b4'),
                 best_val_at=(ax_best_val_at, 'Best value at', '#2ca02c'),
                 steps=(ax_steps, 'Steps', '#2ca02c'),
                 epoch_time=(ax_time, 'Epoch time', '#ff7f0e'),
                 avg_step_time=(ax_step_time, 'Average step time', '#ff7f0e'))

    for key, (ax, title, color) in plots.items():
        stats = stats_dict(key)
        ax.set(title=title + str_fmt.format(**stats))
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        epoch_data[key].hist(ax=ax, legend=False, grid=False,
                             edgecolor='k', alpha=0.7, color=color,
                             bins=ceil(epoch_data[key].nunique() / 4.0))
        ax.axvline(stats['median'], color='k', linewidth=1)
        ax.axvline(stats['mean'], color='k', linestyle='dashed', linewidth=1)

    fig.savefig(filename)


def plot_times(log_data, filename):
    max_steps = log_data['step'].max()
    fig, ax_time = plt.subplots(figsize=(6 + max_steps / 20, 6))
    ax_time.set(xlabel='steps', ylabel='seconds', title='Average execution times')

    means = dict(forward_time=('forward_time', 'mean'),
                 backward_time=('backward_time', 'mean'),
                 optimizer_time=('optimizer_time', 'mean'),
                 eval_time=('eval_time', 'mean'))
    stds = dict(forward_time=('forward_time', 'std'),
                backward_time=('backward_time', 'std'),
                optimizer_time=('optimizer_time', 'std'),
                eval_time=('eval_time', 'std'))
    times_avg = log_data.groupby('step').aggregate(**means)
    times_std = log_data.groupby('step').aggregate(**stds)
    # fill nans if epochs = 1:
    times_std = times_std.fillna(0)
    elinewidth = min(100 / max_steps, 1)
    times_avg.plot.bar(ax=ax_time, stacked=True, yerr=times_std, lw=0, zorder=3,
                       error_kw=dict(elinewidth=elinewidth))
    ticks = [10 * i for i in range(1, ceil(max_steps / 10))]
    ax_time.set_xticks(ticks=[i - 1 for i in ticks])
    ax_time.set_xticklabels(labels=ticks, rotation=0)
    ax_time.grid(zorder=0)
    ax_time.legend(['forward', 'backward', 'optimizer', 'eval'])
    fig.savefig(filename)
