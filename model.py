import numpy as np
import torch
from patrolling_strategy_evaluation import Game
from termcolor import cprint
from torch.nn.functional import normalize


class PatrollingGame(torch.nn.Module):
    def __init__(self, graph, rounding_threshold,
                 evaluator_params=None,
                 loss_params=None,
                 checkpoint=None, verbose=False):
        super().__init__()
        evaluator_params = evaluator_params or {}
        loss_params = loss_params or {}
        self.verbose = verbose
        evaluator_params = {**evaluator_params, 'eps': loss_params.get('eps', 1.0)}
        self.graph = graph
        self.evaluator = Evaluator(self.graph, **evaluator_params, verbose=verbose)
        self.mask = self.evaluator.mask
        self.loss = PNormClampLoss(**loss_params)
        self.threshold = rounding_threshold

        strategy_par = self.mask * torch.log(torch.rand_like(self.mask, dtype=torch.float64))
        self.strategy_par = torch.nn.Parameter(strategy_par)

        if checkpoint:
            self.load_state_dict(state_dict=torch.load(checkpoint))

    def eval(self):
        with torch.no_grad():
            x = self.strategy_par
            x = torch.exp(x)
            x = x * self.mask
            x = normalize(x, p=1, dim=1)
            x = torch.threshold(x, threshold=self.threshold, value=0.0)
            strategy = normalize(x, p=1, dim=1)
            _, val = self.evaluator(strategy)
            return val.item(), strategy.numpy()

    def forward(self):
        x = self.strategy_par
        x = torch.exp(x)
        x = x * self.mask
        x = normalize(x, p=1, dim=1)
        steal, val = self.evaluator(x)
        if self.verbose:
            print_steal(steal)
        loss = self.loss(steal)
        return loss, val.item()


class Evaluator(torch.nn.Module):
    def __init__(self, graph, eps, path_merge_threshold=0.0, verbose=False):
        super().__init__()
        self.verbose = verbose
        self.eps = eps
        self.path_merge_threshold = path_merge_threshold
        self.game = Game(**graph.get_raw_data())
        self.max_value = graph.max_val
        self.mask = self.game.get_mask()

    def forward(self, strategy):
        steal = EvaluateStrategy.apply(strategy, self.game, self.eps, self.path_merge_threshold, self.verbose)
        val = self.max_value * (1.0 - steal[0])
        return steal, val


def print_steal(steals):
    cprint('steals:', 'blue')
    for steal in steals:
        print(f' {steal:.5f}')


def print_grad(steal_grads):
    cprint('steal_grads:', 'blue')
    with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
        for grad in steal_grads:
            print(' ', np.array2string(grad, prefix='  '), '\n')


class EvaluateStrategy(torch.autograd.Function):
    @staticmethod
    def forward(ctx, strategy, game, eps, path_merge_threshold, verbose):
        steal = game.eval_strategy(strategy.detach(), eps, path_merge_threshold)
        ctx.game = game
        ctx.verbose = verbose
        return steal

    @staticmethod
    def backward(ctx, loss_grad):
        steal_grad = ctx.game.compute_gradient(loss_grad)
        if ctx.verbose:
            print_grad(steal_grad)
        return steal_grad, None, None, None, None


class PNormClampLoss(torch.nn.Module):
    def __init__(self, eps, ord):
        super().__init__()
        self.eps = eps
        self.ord = ord

    def forward(self, steal):
        max_steal = torch.max(steal)
        max_steal_static = max_steal.detach()
        steal = (steal - max_steal_static + self.eps) / self.eps
        steal = torch.clamp(steal, min=0.0)
        loss = torch.sum(steal ** self.ord)
        return loss

