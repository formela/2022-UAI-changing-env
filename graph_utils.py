import os
import networkx
import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from termcolor import cprint
from graph_collections import graphs_dict
from graph_modifiers import graph_modifier_from_string


def graph_loader_from_string(loader):
    return graphs_dict[loader]


class PatrollingGraph(object):
    def __init__(self, loader, loader_params, results_dir=None, modifiers=None, verbose=False, name=None):
        self.verbose = verbose
        self.results_dir = results_dir
        self.name = name

        self.graph = graph_loader_from_string(loader)(**loader_params)
        modifiers = modifiers or {}
        floyd_warshall = {'floyd_warshall': {'visiting_bonus': 0.001}}
        modifiers = {**floyd_warshall, **modifiers}
        if verbose:
            cprint('Graph preprocessing:', 'green')
        for modifier, modifier_params in modifiers.items():
            if verbose:
                cprint(f' {modifier}', 'green')
            self.graph = graph_modifier_from_string(modifier)(self.graph, **modifier_params, verbose=verbose)
        if verbose:
            print('done')

        self.print_graph()
        self.graph = self.graph.to_directed()
        self.max_val = max(val for _, val in self.graph.nodes.data('value', default=0))

        self.memory_t = []
        self.memory_n = []
        self.attack_len = []
        self.value = []
        self.blindness = []
        self.targets = []
        self.nodes = []

        for (node, att) in self.graph.nodes.data():
            if att['target']:
                self.targets.append(node)
                self.memory_t.append(att['memory'])
                self.attack_len.append(att['attack_len'])
                self.value.append(att['value'])
                self.blindness.append(att['blindness'])
            else:
                self.nodes.append(node)
                self.memory_n.append(att['memory'])

        self.voc = {node: index for index, node in enumerate(self.targets + self.nodes)}
        self.edges = [(self.voc[a], self.voc[b], att['len']) for a, b, att in self.graph.edges.data()]

        self.aug_nodes = []
        for node in self.targets + self.nodes:
            self.aug_nodes += [(node, mem) for mem in range(self.graph.nodes[node]['memory'])]

        self.aug_node_map = {node: i for i, node in enumerate(self.aug_nodes)}

        self.aug_edges = []
        for source, target in self.graph.edges():
            source_memory = self.graph.nodes[source]['memory']
            target_memory = self.graph.nodes[target]['memory']
            mem_product = product(range(source_memory), range(target_memory))
            self.aug_edges += [((source, ms), (target, mt)) for ms, mt in mem_product]

    def __repr__(self):
        return self.name or self.graph.name or 'PatrollingGraph'

    def get_raw_data(self):
        data = {'memory': self.memory_t + self.memory_n,
                'attack_len': self.attack_len,
                'value': self.value,
                'blindness': self.blindness,
                'edges': self.edges}
        return data

    def print_strategy(self, strategy, style='label', precision=3):
        cprint('Strategy:', 'blue')
        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(' ', np.array2string(strategy, prefix='  '))
        else:
            for (source, ms), (target, mt) in self.aug_edges:
                source_index = self.aug_node_map[(source, ms)]
                target_index = self.aug_node_map[(target, mt)]
                prob = strategy[source_index, target_index]
                if prob > 0:
                    if style == 'label':
                        print(f'  {source} ({ms})--{target} ({mt}):\t{prob:.{precision}f}')
                    elif style == 'index':
                        print(f'  {source_index}--{target_index}:\t{prob:.{precision}f}')
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')

    def print_graph(self):
        if self.verbose:
            cprint(f'Nodes: ({self.graph.number_of_nodes()})', 'blue')
            for node in self.graph.nodes.data():
                print('  ', node)
            cprint(f'Edges: ({self.graph.number_of_edges()})', 'blue')
            for edge in self.graph.edges.data():
                print('  ', edge)
            with np.printoptions(precision=1, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(np.array2string(networkx.to_numpy_matrix(self.graph, weight='len')))
            lengths = [l for _, _, l in self.graph.edges.data('len')]
            cprint('Graph statistics:', 'blue')
            print(f'  edge length: min={min(lengths)}\tmax={max(lengths)}\tavg={np.mean(lengths)}')

        if self.results_dir:
            networkx.draw(self.graph, with_labels=True)
            plt.savefig(os.path.join(self.results_dir, 'graph.pdf'))
