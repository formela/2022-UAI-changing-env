from collections import defaultdict
import random
import networkx


def floyd_warshall(graph, visiting_bonus, verbose=False):
    dist = defaultdict(lambda: defaultdict(lambda: float("inf")))
    for s, t, length in graph.edges.data('len'):
        dist[s][t] = min(length - visiting_bonus, dist[s][t])

    for w in graph:
        dist_w = dist[w]
        for u in graph:
            dist_u = dist[u]
            for v in graph:
                d = dist_u[w] + dist_w[v]
                if d < dist_u[v]:
                    dist_u[v] = d

    edges_to_remove = []
    for s, t, length in graph.edges.data('len'):
        if dist[s][t] < length - visiting_bonus:
            edges_to_remove.append((s, t))
            if verbose:
                print(f'  Removing edge: {s}--{t}')
    graph.remove_edges_from(edges_to_remove)
    return graph


def delete_k_rnd_edges(graph, k, seed=None, verbose=False):
    graph = networkx.DiGraph(graph)
    edge_list = list(graph.edges(data=True))

    if seed is not None:
        random.seed(seed)

    while k > 0:
        chosen_edge = random.choice(edge_list)
        graph.remove_edge(chosen_edge[0], chosen_edge[1])
        if networkx.is_strongly_connected(graph):
            k -= 1
            edge_list.remove(chosen_edge)
            if verbose:
                print(f'  Removing edge: {chosen_edge[0]}--{chosen_edge[1]}')
        else:
            graph.add_edges_from([chosen_edge])
    return graph


def shake_costs(graph, prob, shift, seed=None, verbose=False):
    if seed is not None:
        random.seed(seed)

    for node, att in graph.nodes(data=True):
        if att['target'] and random.uniform(0, 1) < prob:
            sign = random.choice([-1, 1])
            att['value'] *= (1.0 + sign*shift)
            att['value'] = max(0, att['value'])
            if verbose:
                print(f'  Changing value of {node}')

    return graph


def shake_edge_lengths(graph, prob, shift, seed=None, verbose=False):
    if seed is not None:
        random.seed(seed)

    for s, t, att in graph.edges(data=True):
        if random.uniform(0, 1) < prob:
            sign = random.choice([-1, 1])
            att['len'] *= (1.0 + sign*shift)
            att['len'] = max(0, att['len'])
            if verbose:
                print(f'  Changing length of {s}--{t}')

    return graph


def truncated_rands(sigma):
    while True:
        res = random.gauss(0, sigma)
        if -4 * sigma < res < 4 * sigma:
            return res


def graph_modifier_from_string(modifier):
    dct = {'floyd_warshall': floyd_warshall,
           'shake_costs': shake_costs,
           'shake_edge_lengths': shake_edge_lengths,
           'delete_k_rnd_edges': delete_k_rnd_edges}
    return dct[modifier]
